
library(tidyverse)
library(magrittr)
library(lmptools)
library(extrafont)

votes <- map_dfr(list.files('noms', pattern = 'xlsx$', full.names = T), function(file){
  readxl::read_xlsx(file) %>%
    rename(nom = `Proposition de nom`, mention = `Mention`) %>%
    mutate(mention = ifelse(is.na(mention), 'lisse', mention)) %>% 
    spread(key = nom, value = mention)
}) %>% 
  write_delim(delim = "\t", path = 'noms.tsv')


votes_long <- map_df(list.files('noms', pattern = 'xlsx$', full.names = F), function(file){
  on.exit(setwd('..'))
  setwd('noms')
  readxl::read_xlsx(file) %>%
    rename(nom = `Proposition de nom`, mention = `Mention`) %>%
    mutate(mention = ifelse(is.na(mention), 'lisse', mention),
           voter = gsub(pattern = ').xlsx', replacement = '', fixed = T, file),
           voter = gsub(pattern = 'votes noms (', replacement = '', fixed = T, voter),
           voter = ifelse(voter == 'votes noms.xlsx', 0, voter),
           voter = as.integer(voter))
})
unique(votes_long$mention)
order <- c('stratosphérique', 'étoilé', 'éclairci', 'lisse', 'brumeux', 'abyssal', 'apolcayptique')
votes_long <- votes_long %>%
  mutate(ordered_mention = factor(mention, levels = order),
         cardinal_mention = 4 - as.numeric(ordered_mention))
condensed_long <- table(votes_long$ordered_mention, votes_long$nom) %>%
  as.data.frame()

condensed_long %>%
  spread(key = Var2, value = Freq) %>%
  rename(mention = Var1) %T>%
  readr::write_excel_csv('summary_results.csv')

result <- votes_long %>% group_by(nom) %>%
  summarise(result = median(cardinal_mention),
            result_mean = mean(cardinal_mention)) %>% 
  arrange(desc(result))

condensed_long %<>% 
  mutate(Var2 = factor(left(Var2, 40), levels = rev(left(result$nom, 40))))

colors = colorRampPalette(colors = c(
  '#4E8DB6', '#9CCFA7', '#DEEDA4', '#FFFEC6', '#F4BF7A', '#E57A51', '#C84E52'))

plot <- ggplot(condensed_long, aes(x = Var2, y = Freq, fill = Var1), color = NA) +
  geom_bar(position = position_stack(), stat = 'identity') +
  coord_flip() + labs(y = 'Nombre de votes', x = '') +
  geom_hline(yintercept = 4.5, color = '#666666') +
  annotate('text', label = 'médiane', x = 23, y = 4.8, angle = -90, size = 6, color = '#666666') +
  scale_fill_manual(values = colors(7), name = '') +
  scale_y_continuous(breaks = 0:14, labels = 0:14) +
  theme(
    text = element_text(family = "Lato", size = 12),
    legend.position = 'right',
    panel.background = element_blank(),
    plot.background = element_blank(),
    axis.ticks = element_blank())
plot
ggsave("noms/result.png", plot, dpi = 300, width = 16, height = 9)
